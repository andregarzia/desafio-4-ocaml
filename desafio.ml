(* 
  Desafio 04: Mais info em https://osprogramadores.com/desafios/d04/
  
  Autor: Andre Alves Garzia <andre@andregarzia.com>

  Objetivo: Contar as peças de tabuleiro de Xadrez.

  PS: Eu não sei OCaml...
*)
open Core

(* converte string -> chars *)
let tabuleiro_para_chars s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) (s.[i] :: l) in
    exp (String.length s - 1) []

(* retorna 0 ou 1 dependendo se a peça encontrada for a desejada *)
let verifica_peca peca_atual peca_desejada =
  let peca_str = String.of_char peca_atual in
  let r = compare peca_str peca_desejada in
    match r with  (* isso é um pattern matching *)
      0 -> 1
    | _ -> 0
 
(* imprime o relatorio de saida *)
let calcula_saida entrada =
  let chars = tabuleiro_para_chars entrada in
  let peoes = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "1")) chars in
  let bispos = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "2")) chars in
  let cavalos = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "3")) chars in
  let torres = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "4")) chars in
  let rainhas = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "5")) chars in
  let reis = List.fold ~init: 0 ~f:(fun acc p -> acc + (verifica_peca p "6")) chars in
  
    printf "peoes: %d\n" peoes;
    printf "bispos: %d\n" bispos;
    printf "cavalos: %d\n" cavalos;
    printf "torres: %d\n" torres;
    printf "rainhas: %d\n" rainhas;
    printf "reis: %d\n" reis
    
(* abre o arquivo e passa o controle para a funcao anterior *)
let processa file =
  let entrada = In_channel.with_file file ~f:(fun ic -> In_channel.input_all ic) in
    print_endline "Entrada:";
    print_endline entrada;
    printf "\nSaida:\n";
    calcula_saida entrada

(* especificacao do parametro de linha de comando *)
let spec =
  let open Command.Spec in
    empty
    +> anon ("arquivo" %: string)

(* especificacao das opcoes de linha de comando *)
let cli =
  Command.basic
    ~summary:"Calcula desafio 04"
    ~readme:(fun () -> "Mais info em https://osprogramadores.com/desafios/d04/")
    spec
    (fun filename () -> processa filename)

(* roda o app*)
let () =
  Command.run ~version:"1.0" ~build_info:"AAG" cli
