## Desafio 04

Para [esse desafio 04](https://osprogramadores.com/desafios/d04/) você precisa de:

* [OCaml](http://ocaml.org) - A Linguagem
* [OPAM](http://opam.ocaml.org) - O gerenciador de pacotes

Depois precisa rodar o ```setup.sh``` para instalar os módulos ```core``` e ```jbuilder```

Para compilar rode:

```
$ jbuilder build desafio.exe
```

É isso mesmo, tem .exe em qualquer plataforma, mesmo Linux.

Para rodar use:

```
$ ./_build/default/desafio.exe ./caso2.txt
```
